import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.ResponseObject
import web_package.HRMSelectors

/*
 * Uruchomienie modułu admin
 */
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_INDEX/lnk_admin_module'))
/*
 * Uruchomienie opcji JOB menu
 */
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_ADMIN_MODULE/lnk_admin_job_menu'))
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_ADMIN_MODULE/lnk_job_titles'))
/*
 * UUruchomienie opcji - dodanie nowej ofety pracy
 */
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_ADMIN_MODULE/button_add_job_titles'))
/*
 * Wpisanie wartosci w polu job title
 */
WebUI.waitForElementClickable(findTestObject('Object Repository/ORANGE_HRM/Page_ADMIN_MODULE/input_job_title'), 5)
WebUI.setText(findTestObject('Object Repository/ORANGE_HRM/Page_ADMIN_MODULE/input_job_title'), job_title)
/*
 * Wpisanie wartosci w polu job description
 */
WebUI.setText(findTestObject('Object Repository/ORANGE_HRM/Page_ADMIN_MODULE/textarea_job_desc'), job_desc)
/*
 * Wpisanie wartosci w polu job note
 */
WebUI.setText(findTestObject('Object Repository/ORANGE_HRM/Page_ADMIN_MODULE/textarea_job_note'), job_note)
/*
 * Uruchomienie opcji zapis oferty pracy
 */
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_ADMIN_MODULE/button_save_job'))
/*
 * Sprawdzenie oferty pracy
 */
// tworzenie dynamicznego xpatha dla job title
String xpath_job_title = String.format(HRMSelectors.JOB_TITLE, job_title)
//print xpath_job_title # for debug
TestObject obj_lnk_job_title = new TestObject("str_job_title")
obj_lnk_job_title.addProperty("xpath",ConditionType.EQUALS, xpath_job_title) 

job_title_element = WebUI.getText(obj_lnk_job_title)
assert job_title == job_title_element
/*
 * Uruchomienie opcji usuniecie oferty pracy (zaznaczenie/usuniecie/potwierdzenie)
 */
// tworzenie dynamicznego xpatha dla opcji delete job title
String xpath_del_job_title = String.format(HRMSelectors.DEL_JOB_TITLE,job_title) 
TestObject obj_btn_del_job_title = new TestObject("btn_del_job_title")
obj_btn_del_job_title.addProperty("xpath",ConditionType.EQUALS, xpath_del_job_title )

WebUI.waitForElementClickable(obj_btn_del_job_title, 5)
WebUI.click(obj_btn_del_job_title)
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_ADMIN_MODULE/input_dialog_box_ok'))
