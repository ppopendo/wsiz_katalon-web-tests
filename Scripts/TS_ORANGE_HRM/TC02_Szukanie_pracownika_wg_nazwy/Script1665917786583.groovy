import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
/*
 * Uruchomienie modulu PIM
 */
WebUI.mouseOver(findTestObject('Object Repository/ORANGE_HRM/Page_INDEX/lnk_pim_module'))
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_INDEX/lnk_pim_module'))
/*
 * Wpisanie wartosci employee_name w polu Employee Name
 */
WebUI.setText(findTestObject('Object Repository/ORANGE_HRM/Page_PIM_MODULE/input_employee_name'), employee_name_search)
/*
 * Uruchomienie wyszukiwania pracownika
 */
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_PIM_MODULE/button_search'))
/*
 * Wyswietlenie opisu pracownika
 */
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_PIM_MODULE/lnk_id_employee'))
/*
 * Pobranie nazwy pracownika
 */
WebUI.waitForElementClickable(findTestObject('Object Repository/ORANGE_HRM/Page_PIM_MODULE/str_employee_lastname'), 10)
el_employee_name = WebUI.getText(findTestObject('Object Repository/ORANGE_HRM/Page_PIM_MODULE/str_employee_lastname'))
/*
 * Weryfikacja nazwy uzytkownika na profilu
 */
assert employee_name_expected == el_employee_name
