import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
/*
 * Uruchomienie menu uzytkownika
 */
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_INDEX/str_username'))
/*
 * Uruchomienie menu about
 */
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_INDEX/lnk_about'))
/*
 * Pobranie infomracje o nazwie firmy
 */
el_company_name = WebUI.getText(findTestObject('Object Repository/ORANGE_HRM/Page_INDEX/str_company_name'))
/*
 * Weryfikacja informacji o nazwie firmy
 */
assert company_name == el_company_name
/*
 * Pobranie infomracje o wersji aplikacji
 */
el_version_app = WebUI.getText(findTestObject('Object Repository/ORANGE_HRM/Page_INDEX/str_version_app'))
/*
 * Weryfikacja informacji o wersji aplikacji
 */
assert version_app == el_version_app
/*
 * Zamykanie menu "about"
 */
WebUI.click(findTestObject('Object Repository/ORANGE_HRM/Page_INDEX/btn_close_menu_about'))
