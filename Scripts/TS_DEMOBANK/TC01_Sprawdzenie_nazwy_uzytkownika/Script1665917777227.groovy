import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

/*
 * Pobranie nazwy klienta
 */
String customer_name_elem = WebUI.getText(findTestObject('Object Repository/DEMOBANK/PageDesktop/lnk_customer_name'))
/*
 * Sprawdzenie nazwy klienta
 */
assert customer_name == customer_name_elem
