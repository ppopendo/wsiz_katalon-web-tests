import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.model.FailureHandling as FailureHandling


/*
 * Uruchomienie przegladarki 
*/
WebUI.openBrowser(GlobalVariable.uri_login)

WebUI.takeScreenshotAsCheckpoint('LoginPage')

/*
 * Zmiana rozmiaru okna przegladarki - maksymalny rozmiar
*/
WebUI.maximizeWindow()

WebUI.takeElementScreenshotAsCheckpoint('LoginForm',findTestObject('Object Repository/DEMOBANK/PageLogin/login_form'))
/*
 * Wpisanie nazwy uzytkownika
 */
WebUI.setText(findTestObject('Object Repository/DEMOBANK/PageLogin/inp_login_id'), user_login)

WebUI.takeElementScreenshotAsCheckpoint('LoginForm-with_login_data',findTestObject('Object Repository/DEMOBANK/PageLogin/login_form'))
/*
 * Uruchomienie opcji "dalej"
 */
WebUI.click(findTestObject('Object Repository/DEMOBANK/PageLogin/btn_login_next'))

/*
 * Wpisanie hasła uzytkownika
 */
WebUI.setText(findTestObject('Object Repository/DEMOBANK/PageLogin/inp_login_password'), user_pwd)

WebUI.takeElementScreenshotAsCheckpoint('LoginForm-with_login_and_pwd_data',findTestObject('Object Repository/DEMOBANK/PageLogin/login_form'))
WebUI.takeScreenshotAsCheckpoint('LoginPage-with_data')

/*
 * Uruchomienie opcji "zaloguj sie"
 */
WebUI.click(findTestObject('Object Repository/DEMOBANK/PageLogin/btn_login'))
