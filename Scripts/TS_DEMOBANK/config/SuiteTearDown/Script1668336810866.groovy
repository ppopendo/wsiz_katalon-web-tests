import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

/*
 * Wylgoowanie uzytkownika
 */
WebUI.click(findTestObject('Object Repository/DEMOBANK/PageDesktop/lnk_logout'))
/*
 * Zamykanie przegladarki
 */
WebUI.closeBrowser()
 