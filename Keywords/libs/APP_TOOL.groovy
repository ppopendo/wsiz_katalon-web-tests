package libs
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windowsimport java.time.OffsetDateTime as OffsetDateTime
import java.time.ZoneOffset as ZoneOffset
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder as DateTimeFormatterBuilder
import java.util.Locale as Locale
import internal.GlobalVariable

public class APP_TOOL {
	/*
	 * get the timestamp for given offset and language
	 */
	public def get_timestamp(ZoneOffset zone_offset = ZoneOffset.of('+00:00'), Locale language = Locale.ENGLISH) {
		OffsetDateTime time_now = OffsetDateTime.now(zone_offset)
		DateTimeFormatter date_time_format = new DateTimeFormatterBuilder().parseCaseInsensitive()
				.appendPattern('yyyymmddHHmmss').toFormatter(language)
		return date_time_format.format(time_now)
	}
}
