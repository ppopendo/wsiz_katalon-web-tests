package api_package
public class JiraData {
	/*
	 * JSON data to create/update object in Jira
	 */

	/*
	 * JSON data for adding a new quality issue
	 */
	public static final String QUALITY_ISSUE__NEW = '''{
				"fields": {
					"summary": "%s",
					"parent": {},
					"issuetype": {
						"id": "%s"
					},
					"project": {
						"id": "%s"
					},
					"description": {
						"type": "doc",
						"version": 1,
						"content": [
							{
								"type": "paragraph",
								"content": [
									{
										"text": "%s",
										"type": "text"
									},
									{
										"type": "hardBreak"
									},
									{
										"text": "%s",
										"type": "text"
									},
									{
										"type": "hardBreak"
									},
									{
										"text": "%s",
										"type": "text"
									}
								]
							}
						]
					},
					"reporter": {
						"id": "557058:f4714334-e3f9-4b6c-8265-8feb4ca004ba"
					},
					"priority": {
						"id": "3"
					},
					"assignee": {
						"id": null
					}
				}
			}
		}'''

	/*
	 * JSON data for adding a new quality issue
	 */
	public static final String ISSUE_LINK__NEW = '''{
			"type": {
				"id": "%s"
			},
			"inwardIssue": {
				"key": "%s"
			},
			"outwardIssue": {
				"key": "%s"
			}
		}'''
	/*
	 * JSON data for update issue status
	 */
	public static final String ISSUE_STATUS__UPDATE = '''{
	"transition": {
		"id": "%s"
	}
}'''
}