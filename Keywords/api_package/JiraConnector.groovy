package api_package
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.RestRequestObjectBuilder
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

class JiraConnector {
	String uri = "https://wsiztesting.atlassian.net/"
	String resource = "rest/api/3/"
	String authHeader = "Basic please update user_id and jira token in base64"
	/*
	 * request header
	 */
	TestObjectProperty header1 = new TestObjectProperty("Authorization", ConditionType.EQUALS, authHeader)
	TestObjectProperty header2 = new TestObjectProperty("Content-Type", ConditionType.EQUALS, "application/json")
	TestObjectProperty header3 = new TestObjectProperty("Accept", ConditionType.EQUALS, "application/json")
	ArrayList default_headers = Arrays.asList(header1, header2, header3)
	/*
	 * GET data from JIRA
	 */
	public ResponseObject get_data(String name_resource) {
		RequestObject obj_request = new RequestObject("objectId")
		obj_request.setRestUrl(uri + resource + name_resource)
		obj_request.setHttpHeaderProperties(default_headers)
		obj_request.setRestRequestMethod("GET")
		ResponseObject obj_response = WS.sendRequest(obj_request)
		return obj_response
	}
	/*
	 * send request to JIRA
	 */
	public ResponseObject send_request(String name_resource, String body_msg) {
		RequestObject obj_request = new RestRequestObjectBuilder()
				.withRestUrl(uri + resource + name_resource)
				.withHttpHeaders(default_headers)
				.withRestRequestMethod("POST")
				.withTextBodyContent(body_msg)
				.build()
		ResponseObject obj_response = WS.sendRequest(obj_request)
		return obj_response
	}
}