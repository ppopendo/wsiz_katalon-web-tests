package web_package

class HRMSelectors {
	/*
	 * Dynamic selectors for Orange HRM 
	 */
	public static final String JOB_TITLE = '//div[@class="oxd-table"]//div[@class="oxd-table-card"]//div[contains(text(),"%s")]'
	public static final String DEL_JOB_TITLE = '//div[contains(text(),"%s")]/parent::div/following-sibling::div[2]//button[1]'
}