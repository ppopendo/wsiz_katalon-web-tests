# Testy automatyczne dla web aplikacji z wykorzystaniem Katalon Studio
 

### aktualizacja 2022-11-13

## Opis projektu 
* [Informacje](#informacje)
* [Technologie](#technologie)
* [Konfiguracja](#konfiguracja)

## Informacje
Projekt zwiera przykładowe skrypty do nauki automatyzacji testów z wykorzystaniem Katalon Studio
	
## Technologie
* Katalon Studio
* Rest service
* Groovy
	
## Konfiguracja
1. Pobranie Katalon Studio: [KatalonStudio](https://katalon.com/ "KatalonStudio")
2. Rejestracja konta w JIRA: [JIRA](https://www.atlassian.com/pl/software/jira "JIRA")

## Tabela z opisem skryptów 

APLIKACJA | ZESTAW TESTOW | PRZYPADEK TESTOWY | STATUS
------------ | ------------- | ------------- | ------------- 	
WSIZ | TS_WSIZ_ELEARNING | TC01_Elearning_logowanie_bledne_dane | :heavy_check_mark:	
WSIZ | TS_WSIZ_ELEARNING | TC01_Elearning_logowanie_bledne_dane2 | :heavy_check_mark:	
ORANGE HRM | TS_ORANGE_HRM  | TC01_Sprawdzenie_menu_uzytkownika | :heavy_check_mark:	
ORANGE HRM | TS_ORANGE_HRM  | TC02_Szukanie_pracownika_wg_nazwy | :heavy_check_mark:	
ORANGE HRM | TS_ORANGE_HRM  | TC03_Dodanie_oferty_pracy | :heavy_check_mark:
Demobank | TS_DEMOBANK | TC01_Sprawdzenie_nazwy_uzytkownika | :heavy_check_mark:	
