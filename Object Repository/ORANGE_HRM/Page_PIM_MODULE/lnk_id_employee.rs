<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Id pracownika z tabeli (pierwszy rekord)</description>
   <name>lnk_id_employee</name>
   <tag></tag>
   <elementGuidId>d484d2f4-6626-48dd-8379-e1c5525df03d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button>i[class=&quot;oxd-icon bi-pencil-fill&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
