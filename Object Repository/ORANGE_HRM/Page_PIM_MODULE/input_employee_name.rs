<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Pole do wyszukiwania nazwy pracownika</description>
   <name>input_employee_name</name>
   <tag></tag>
   <elementGuidId>ba1ca5ec-abc1-4e7a-9051-49233bb419e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;oxd-grid-item oxd-grid-item--gutters&quot;][1]//input</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
