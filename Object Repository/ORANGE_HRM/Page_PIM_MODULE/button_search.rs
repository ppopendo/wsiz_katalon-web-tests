<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Przycisk do wyszukiwania pracowników</description>
   <name>button_search</name>
   <tag></tag>
   <elementGuidId>241353c8-0cc3-41e6-9f68-11648db8bfad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button[class=&quot;oxd-button oxd-button--medium oxd-button--secondary orangehrm-left-space&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
