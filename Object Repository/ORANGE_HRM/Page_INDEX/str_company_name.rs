<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>About - > company name</description>
   <name>str_company_name</name>
   <tag></tag>
   <elementGuidId>42f59eae-10aa-47f7-a521-5bdc7213742d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div[class=&quot;oxd-grid-2 orangehrm-about&quot;]>div:nth-of-type(2)>p</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
