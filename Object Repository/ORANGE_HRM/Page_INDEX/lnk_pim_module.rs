<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>PIM module</description>
   <name>lnk_pim_module</name>
   <tag></tag>
   <elementGuidId>680dafe3-670f-4262-8f15-ad6760ad9794</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul[class=&quot;oxd-main-menu&quot;]>li:nth-of-type(2)>a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
