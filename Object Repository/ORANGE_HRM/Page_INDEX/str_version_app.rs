<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Wersja aplikacji</description>
   <name>str_version_app</name>
   <tag></tag>
   <elementGuidId>6134ea98-7aa8-4f66-883f-b937a5a6f187</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div[class=&quot;oxd-grid-2 orangehrm-about&quot;]>div:nth-of-type(4)>p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
