<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Wylogowanie uzytkownika</description>
   <name>lnk_logout</name>
   <tag></tag>
   <elementGuidId>469aa602-f664-4e1b-897d-2c5314362dae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul[class=&quot;oxd-dropdown-menu&quot;]>li:nth-of-type(4)>a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
