<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Moduł administratora</description>
   <name>lnk_admin_module</name>
   <tag></tag>
   <elementGuidId>183f17b3-78a2-4cd4-82b5-463957dee205</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul[class=&quot;oxd-main-menu&quot;]>li:nth-of-type(1)>a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
