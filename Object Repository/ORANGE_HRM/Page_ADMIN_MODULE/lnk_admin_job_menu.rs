<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Admin module menu job</description>
   <name>lnk_admin_job_menu</name>
   <tag></tag>
   <elementGuidId>fddbc1ec-43bd-40c2-8019-04e2592f2784</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>nav[class=&quot;oxd-topbar-body-nav&quot;]>ul>li:nth-of-type(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
