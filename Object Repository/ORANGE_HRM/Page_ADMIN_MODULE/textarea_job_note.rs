<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Notatka oferty pracy</description>
   <name>textarea_job_note</name>
   <tag></tag>
   <elementGuidId>7d6e13c1-6460-4c1c-96a4-079ee2581bfe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@class=&quot;oxd-form&quot;]//div[@class=&quot;oxd-form-row&quot;][4]//textarea</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
