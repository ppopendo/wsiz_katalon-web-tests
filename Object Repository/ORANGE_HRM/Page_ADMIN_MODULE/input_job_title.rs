<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Nazwa oferty pracy</description>
   <name>input_job_title</name>
   <tag></tag>
   <elementGuidId>abd7f4ac-c7ed-4131-bcbd-fb676a9067b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@class='oxd-form']//input[@class='oxd-input oxd-input--active']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
