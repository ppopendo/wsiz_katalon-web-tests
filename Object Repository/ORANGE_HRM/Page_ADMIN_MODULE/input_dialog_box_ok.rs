<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Okno dialogowe - przycisk ok</description>
   <name>input_dialog_box_ok</name>
   <tag></tag>
   <elementGuidId>dfe49178-0cda-43fd-bbdb-412e6110376b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button[class=&quot;oxd-button oxd-button--medium oxd-button--label-danger orangehrm-button-margin&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
