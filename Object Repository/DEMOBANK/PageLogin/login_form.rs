<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Login form</description>
   <name>login_form</name>
   <tag></tag>
   <elementGuidId>2e9d0d66-9c67-4c8a-ae8a-ac1ac249c806</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'login_form']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>login_form</value>
      <webElementGuid>f34f98ab-19f8-405c-9747-e4de044655e0</webElementGuid>
   </webElementProperties>
</WebElementEntity>
