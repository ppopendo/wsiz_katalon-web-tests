<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Nazwa klienta</description>
   <name>lnk_customer_name</name>
   <tag></tag>
   <elementGuidId>531e78c9-9a18-4731-9656-512a920a1943</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'user_name']</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user_name</value>
      <webElementGuid>f5e2ff53-7a73-4441-b816-d1a0c4a7711b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
