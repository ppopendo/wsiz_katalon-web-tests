<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lnk_wsiz_login</name>
   <tag></tag>
   <elementGuidId>f2cd6149-d2e1-4416-90d1-443c326db875</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='page-site-index']/header/nav/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>0c4cbbd2-08bc-48fd-9ccf-de515475c05f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>brand</value>
      <webElementGuid>a67e5dc8-2ba2-4d7f-9c30-c29fb605c70d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Strona główna</value>
      <webElementGuid>8a0480c9-027b-4e1c-aacb-7bde93ce3016</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://elearning.wsiz.wroc.pl/</value>
      <webElementGuid>876ad5a3-64cf-431a-b872-392e466fcc38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>WSIZ Copernicus</value>
      <webElementGuid>fac60b86-cd86-4803-971c-ec96bbbaa030</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-site-index&quot;)/header[@class=&quot;navbar navbar-fixed-top navbar-inverse moodle-has-zindex&quot;]/nav[@class=&quot;navbar-inner&quot;]/div[@class=&quot;container-fluid&quot;]/a[@class=&quot;brand&quot;]</value>
      <webElementGuid>4642eb4e-0df0-4816-b490-387e665d42e0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='page-site-index']/header/nav/div/a</value>
      <webElementGuid>9e74ac4a-5664-49b0-a44c-f9aa009a84b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'WSIZ Copernicus')]</value>
      <webElementGuid>196ac3ca-5cad-49d4-96ee-117d6af8b9b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Przejdź do głównej zawartości'])[1]/following::a[1]</value>
      <webElementGuid>597caa57-9fc7-4e02-9d48-bdbf45bbd19e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Zaloguj się'])[1]/preceding::a[2]</value>
      <webElementGuid>10f0a0bc-2fa9-4bf8-b4d9-22c7fd954f25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://elearning.wsiz.wroc.pl/']</value>
      <webElementGuid>4133a36c-1cbf-4e3c-a928-4c64ff95c9e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/div/a</value>
      <webElementGuid>07c24737-bf23-4fca-bf11-b2a5ae1ce207</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
