import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext
import web_package.HRMSelectors
import api_package.JiraConnector
import api_package.JiraData
import libs.APP_TOOL
import com.kms.katalon.core.testobject.ResponseObject
import internal.GlobalVariable as GlobalVariable
import java.time.ZoneOffset as ZoneOffset
import com.kms.katalon.core.util.KeywordUtil
import groovy.json.JsonSlurper

class TestTeardown {

	JiraConnector jira_conn = new JiraConnector()
	APP_TOOL app_tools = new APP_TOOL()
	JsonSlurper json_slurper = new JsonSlurper()
	
	/**
	 * Executes before every test case starts.
	 */
	@BeforeTestCase
	def sampleBeforeTestCase(TestCaseContext testCaseContext) {
		println testCaseContext.getTestCaseId()
	}

	/**
	 * Executes after every test case ends.
	 */
	@AfterTestCase
	def test_teardown(TestCaseContext testCaseContext) {
		/*
		 * Status of test case
		 */
		String testcase_status = testCaseContext.getTestCaseStatus()
		/*
		 * Test case message [Stacktrace if test case fails, empty string otherwise] 
		 */
		String [] obj_testcase_msg = testCaseContext.getMessage().split("\r")
		/*
		 * Test case message [Stacktrace if test case fails, empty string otherwise]
		 */
		String [] obj_testname = testCaseContext.getTestCaseId().split("/")
		/*
		 * Test suite name
		 */
		String test_suite_name = obj_testname[1]
		/*
		 * Test case name
		 */
		String test_case_name = obj_testname[2]		
		/*
		 * 1. add new quality issue
		 */
		String summary_msg =  'TC:%s_ID%s - %s'
		summary_msg = String.format(summary_msg,test_case_name, app_tools.get_timestamp(ZoneOffset.of('+01:00')), testcase_status )
		String body_msg = String.format(
			JiraData.QUALITY_ISSUE__NEW,summary_msg,GlobalVariable.jira_issuetype_id, GlobalVariable.jira_project_id, "Test Suite:" + test_suite_name,
			"Test Case: " + test_case_name, "Status: " + testcase_status + " " + obj_testcase_msg[0].replace("\n", " " )
			)
		ResponseObject jira_response = jira_conn.send_request("issue", body_msg )
//		println jira_response.getResponseBodyContent()  //for debug
		if (jira_response.getStatusCode() != 201) {
			KeywordUtil.markFailed("Status code is not 201 as expected. It is: " + jira_response.getStatusCode())
		}
		/*
		 * 2. add link new issue to the story 
		*/
		String issue_key = json_slurper.parseText(jira_response.getResponseBodyContent())['key']
		body_msg = String.format(JiraData.ISSUE_LINK__NEW, "10006",issue_key, GlobalVariable.jira_story_id)
		jira_response = jira_conn.send_request("issueLink", body_msg )
		if (jira_response.getStatusCode() != 201) {
			KeywordUtil.markFailed("Status code is not 201 as expected. It is: " + jira_response.getStatusCode())
		}
		/*
		 * 3. change status issue -> to done
		*/
		body_msg = String.format(JiraData.ISSUE_STATUS__UPDATE, "41",)
		jira_response = jira_conn.send_request("issue/" + issue_key + "/transitions", body_msg)
		if (jira_response.getStatusCode() != 201) {
			KeywordUtil.markFailed("Status code is not 204 as expected. It is: " + jira_response.getStatusCode())
		}
	}
}
